import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Books from './Components/Books';
import Book from './Components/Book';

const MainNavigator = createStackNavigator({
  Books: {screen: Books},
  Book: {screen: Book},
});

const App = createAppContainer(MainNavigator);

export default App;
